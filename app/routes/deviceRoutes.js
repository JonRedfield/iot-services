var express = require('express');
var router = express.Router();
var deviceController = require('../controllers/deviceController');

// Path Paramaters
/**
 * Anytime 'deviceId' is found in a URL, use it to lookup
 * the readings for that device and inject them into the 
 * request.
 */
// app.param('deviceId', function(req, res, next, deviceId) {
// 	req.readings = db.readings(deviceId);
// 	return next();
// });

// Device endpoints
router.get('/devices', deviceController.getAllDevices);
router.post('/devices', deviceController.addDevice);

router.get('/devices/:deviceId', deviceController.getDevice);
router.put('/devices/:deviceId', deviceController.updateDevice);
router.delete('/devices/:deviceId', deviceController.deleteDevice);

module.exports = router;