var app = require('express');

var deviceRoutes = require('./deviceRoutes');
var readingsRoutes = require('./readingsRoutes');

module.exports = [deviceRoutes, readingsRoutes];