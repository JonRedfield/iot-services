var express = require('express');
var router = express.Router();
var readingsController = require('../controllers/readingsController');

router.get('/devices/:deviceId/readings', readingsController.getAllDeviceReadings);
router.post('/readings', readingsController.addDeviceReading);
router.get('/readings/:readingId', readingsController.getReading);

module.exports = router;
