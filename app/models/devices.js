var mongoose = require('mongoose');

var deviceSchema = new mongoose.Schema({
	name: String,
	capabilities: [String],
	mac: String
});

mongoose.model('Device', deviceSchema, 'devices');
