var mongoose = require('mongoose');

var readingSchema = new mongoose.Schema({
	type: String,
	value: Number
});

var readingsSchema = new mongoose.Schema({
	deviceId: mongoose.Schema.Types.ObjectId,
	timestamp: Date,
	location: {
		type: [Number],
		index: '2dsphere'
	},
	readings: [readingSchema]
});

mongoose.model('Reading', readingsSchema, 'readings');
