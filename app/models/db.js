var mongoose = require('mongoose');
var dbUri = 'mongodb://localhost/iot';
mongoose.connect(dbUri);

// Log mongoose events.
mongoose.connection.on('connected', function() {
	console.log('Mongoose conneted to ' + dbUri);
});

mongoose.connection.on('error', function(err) {
	console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function() {
	console.log('Mongoose disconnected');
});

var gracefulShutdown = function(msg, callback) {
	mongoose.connection.close(function() {
		console.log('Mongoose disconnected through ' + msg);
		callback();
	});
};

// Listen for environment (node/OS) shutdown events
process.once('SIGUSR2', function() {
	gracefulShutdown('nodemon restart', function() {
		process.kill(process.pid, 'SIGUSR2');
	});
});


process.on('SIGINT', function() {
	gracefulShutdown('app termination', function() {
		process.exit(0);
	});
});


process.on('SIGTERM', function() {
	gracefulShutdown('Heroku app shutdown', function() {
		process.exit(0);
	});
});


// Bring in all document schemas
require('./devices');
require('./readings');