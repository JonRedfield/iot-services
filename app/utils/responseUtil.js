exports.sendSuccess = function(res, content) {
	sendJsonResult(res, 200, content);
};


exports.sendCreateSuccess = function(res, content) {
	sendJsonResult(res, 201, content);
}


exports.sendDeleteSuccess = function(res, message) {
	sendJsonResult(res, 204, {message: message});
}


exports.sendConflictError = function(res, message) {
	var errorMessage = {
		errorType: '409 - Resource conflict',
		description: message
	};

	sendJsonResult(res, 409, errorMessage);
}


exports.sendNotFoundError = function(res, message) {
	var errorMessage  = {
		errorType: '404 - Resource not found',
		description: message
	};

	sendJsonResult(res, 404, errorMessage);
}


exports.sendBadRequestError = function(res, message) {
	var errorMessage  = {
		errorType: '400 - Bad request',
		description: message
	};

	sendJsonResult(res, 404, errorMessage);
}


exports.sendServerError = function(res, message) {
	var errorMessage = {
		errorType: '500 - Server error',
		description: message
	};

	sendJsonResult(res, 500, errorMessage);
}


var sendJsonResult = function(res, status, content) {
	res.status(status);
	res.json(content);
};