var mongoose = require('mongoose');
var Device = mongoose.model('Device');
var responseUtil = require('../utils/responseUtil');


exports.getAllDevices = function(req, res) {
	Device
	.find()
	.exec(function(err, devices) {
		if (devices) {
			responseUtil.sendSuccess(res, devices);
		} else if (err) {
			responseUtil.sendServerError(res, err);
		}
	});
};


exports.addDevice = function(req, res) {
	Device
	.create(req.body, function(err, device) {
		if (err) {
			responseUtil.sendServerError(res, err);
		} else {
			responseUtil.sendCreateSuccess(res, device);
		}
	})
};


exports.getDevice = function(req, res) {
	if (req.params && req.params.deviceId) {
		Device
		.findById(req.params.deviceId)
		.exec(function(err, device) {
			if (device) {
				responseUtil.sendSuccess(res, device);
			} else if (err) {
				responseUtil.sendServerError(res, err);
			} else {
				responseUtil.sendNotFoundError(res, 'Device [' + req.params.deviceId + '] is not found.');
			}
		});
	} else {
		responseUtil.sendBadRequestError(res, 'DeviceId is required.');
	}
};


exports.updateDevice = function(req, res) {
	Device
	.findById(req.params.deviceId)
	.exec(function(err, device) {
		if (err) {
			responseUtil.sendServerError(res, err);
		} else if (!device) {
			responseUtil.sendNotFoundError(res, 'Device [' + req.params.deviceId + '] not found.');
		} else {
			device.name = req.body.name;
			device.mac = req.body.mac;
			device.capabilities = req.body.capabilities;
			device.save(function(err) {
				if (err) {
					responseUtil.sendServerError(res, err);
				} else {
					responseUtil.sendSuccess(res, device);
				}
			});
		}
	});
};


exports.deleteDevice = function(req, res) {
	var deviceId = req.params.deviceId;
	if (deviceId) {
		Device
		.findByIdAndRemove(deviceId)
		.exec(function(err, device) {
			if (err) {
				responseUtil.sendServerError(res, err);
			}

			responseUtil.sendDeleteSuccess(res, 'Device successfully deleted.');
		});
	} else {
		responseUtil.sendBadRequestError(res, 'DeviceId is required.');
	}
};
