var mongoose = require('mongoose');
var Reading = mongoose.model('Reading');
var responseUtil = require('../utils/responseUtil');


exports.getAllDeviceReadings = function(req, res) {
	if (req.params && req.params.deviceId) {
		var deviceId = req.params.deviceId;
		Reading
		.find({deviceId: deviceId})
		.sort({timestamp: -1})
		.exec(function(err, readings) {
			if (err) {
				responseUtil.sendServerError(res, err);
			} else {
				responseUtil.sendSuccess(res, readings);
			}
		});
	} else {
		responseUtil.sendBadRequestError(res, 'deviceId is required.');
	}
};


exports.getDeviceReadingsByDateRange = function(req, res) {
	responseUtil.sendServerError(res, 'End point not implemented!');
};


exports.addDeviceReading = function(req, res) {
	Reading
	.create(req.body, function(err, reading) {
		if (err) {
			responseUtil.sendServerError(res, err);
		} else {
			responseUtil.sendCreateSuccess(res, reading);
		}
	});
};


exports.getReading = function(req, res) {
	if (req.params && req.params.readingId) {
		var readingId = req.params.readingId;
		Reading
		.findById(readingId)
		.exec(function(err, reading) {
			if (err) {
				responseUtil.sendServerError(res, err);
			} else if (!reading) {
				responseUtil.sendNotFoundError(res, 'Reading [' + readingId + '] does not exist.');
			} else {
				responseUtil.sendSuccess(res, reading);
			}
		});
	} else {
		responseUtil.sendBadRequestError(res, 'readingId is required.');
	}
};
