var express = require('express');
var mongoskin = require('mongoskin');
var bodyParser = require('body-parser');
var logger = require('morgan');
require('./app/models/db.js');


var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(logger('dev'));
app.use('/', require('./app/routes/index'));

var db = mongoskin.db('mongodb://localhost:27017/iot?auto_reconnect', {safe: true});

app.use(function(req, res, next) {
	req.db = {};
	req.db.iot = db.collection('iot');
	next();
});


app.all('*', function(req, res) {
	res.status(404).send();
});



app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'), function() {
	console.log('Listening on port ' + app.get('port'));
});

